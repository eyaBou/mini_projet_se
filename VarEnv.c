#include <stdio.h>
#include <stdlib.h>  /* pour utiliser getenv */

int main(void)
{
   char *valeur;
   valeur = getenv("PATH");
   if (valeur != NULL)
   printf("Le PATH vaut : %s \n", valeur);
   valeur = getenv("HOME");
   if (valeur != NULL)
      printf("Le home directory est dans %s \n", valeur);
   return 0;
}